randomness Randomness module.
==============================================================================
randomness Randomness (C) 2013 by A.Wagner (http://aw030.de)
==============================================================================

randomness Randomness block module which provides a set of randomly moving 
"pseudo particles" such as points, img or a html tag elements.

Installation
------------------------------------------------------------------------------

The installation is very simple:

  I. Installation
  II. Configuration

I. Installation

  1) Moving to module folder:
     Move the downloaded module package to sites/all/modules.
     => sites/all/modules/randomness/
     
  2) Activating the module:
     In administration back-end on the module configuration (admin/modules) 
     page you have now to install/activate the module and save the module 
     configuration.
  
  3) Activating and placing the block:
     After activation of the module e new block is added to the block 
     configuration page under structure/blocks. Now move the new block to 
     the desired region and make some block specific configuration.

II. Configuration

  1) After installation you should check the configuration page for the 
     randomness module and customize the settings. Options are self 
     explaining in the administration form.

------------------------------------------------------------------------------
Module written by A.Wagner on http://drupal.org
------------------------------------------------------------------------------
